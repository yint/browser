package com.yint.browser.bookmark;

import java.util.ArrayList;
import java.util.List;

public class BookMarkFactory {
    private final String TAG = "BookMarkFactory";

    private static final BookMarkFactory factory = new BookMarkFactory();

    private BookMarkFactory() {

    }

    public static BookMarkFactory getFactory() {
        return factory;
    }

    /**
     * 获取书签
     *
     * @return 书签列表
     */
    public List<BookMark> getBookMarks() {
        List<BookMark> bookMarkList = new ArrayList<>();
        bookMarkList.add(new BookMark("百度", "https://m.baidu.com"));
        bookMarkList.add(new BookMark("腾讯视频", "https://m.v.qq.com/index.html"));
        bookMarkList.add(new BookMark("优酷视频", "https://youku.com"));
        bookMarkList.add(new BookMark("哔哩哔哩", "https://m.bilibili.com"));
        bookMarkList.add(new BookMark("爱奇艺", "https://m.iqiyi.com"));
        bookMarkList.add(new BookMark("百度网盘", "https://pan.baidu.com"));
        bookMarkList.add(new BookMark("AGE动漫", "https://www.agefans.net"));

        return bookMarkList;
    }

}

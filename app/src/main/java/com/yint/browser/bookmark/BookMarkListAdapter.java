package com.yint.browser.bookmark;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yint.browser.R;

import java.util.ArrayList;
import java.util.List;

public class BookMarkListAdapter extends BaseAdapter {
    private final List<BookMark> bookMarks = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public BookMarkListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setBookMarks(List<BookMark> bookMarkList) {
        if (bookMarkList != null) {
            bookMarks.clear();
            bookMarks.addAll(bookMarkList);
            notifyDataSetChanged();
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return bookMarks.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_book_mark_layout, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        BookMark bookMark = bookMarks.get(position);
        holder.textView.setText(bookMark.getBookMarkName());
        return convertView;
    }

    public BookMark getItem(int position) {
        BookMark bookMark = null;
        try {
            bookMark = bookMarks.get(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookMark;
    }


    public static class ViewHolder {
        private final TextView textView;

        public ViewHolder(@NonNull View itemView) {
            textView = itemView.findViewById(R.id.text);
        }
    }

}

package com.yint.browser.bookmark;

public class BookMark {
    private int type;
    private String bookMarkName;
    private String url;

    public BookMark(String bookMarkName, String url) {
        this.bookMarkName = bookMarkName;
        this.url = url;
    }

    public BookMark(int type, String bookMarkName, String url) {
        this.type = type;
        this.bookMarkName = bookMarkName;
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBookMarkName() {
        return bookMarkName;
    }

    public void setBookMarkName(String bookMarkName) {
        this.bookMarkName = bookMarkName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "BookMark{" +
                "type=" + type +
                ", bookMarkName='" + bookMarkName + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}

package com.yint.browser;

import java.util.regex.Pattern;

public class urlRegTest {

    public static void main(String[] args) {
        String url1 = "http://www.xx.com";
        String url2 = "w~.xx.com";
        String url3 = "http://w.xx.com";
        String url4 = "ssss";
        String regex="^((https|http|ftp)?://)?(([A-Za-z0-9~]+)\\.)+([A-Za-z0-9~])+(\\/.+\\/)?(\\/.*)?$";
        Pattern pattern = Pattern.compile(regex);
        System.out.println(pattern.matcher(url1).matches());
        System.out.println(pattern.matcher(url2).matches());
        System.out.println(pattern.matcher(url3).matches());
        System.out.println(pattern.matcher(url4).matches());
    }

}
package com.yint.browser;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.yint.browser.bookmark.BookMark;
import com.yint.browser.bookmark.BookMarkFactory;
import com.yint.browser.bookmark.BookMarkListAdapter;
import com.yint.browser.widget.CustomSearchView;

/**
 * 浏览器首页
 */
public class IndexActivity extends BaseActivity {

    private final String TAG = "IndexActivity";

    private CustomSearchView searchView;
    private GridView bookMarkGv;
    private BookMarkListAdapter bookMarkListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        initView();
        loadBookMarks();
    }

    private void initView() {
        searchView = findViewById(R.id.search_view);
        bookMarkGv = findViewById(R.id.book_mark_gv);

        bookMarkListAdapter = new BookMarkListAdapter(this);
        bookMarkGv.setAdapter(bookMarkListAdapter);

        searchView.setOnSearchListener(onSearchListener);
        bookMarkGv.setOnItemClickListener(onItemClickListener);
    }

    private void loadBookMarks() {
        bookMarkListAdapter.setBookMarks(BookMarkFactory.getFactory().getBookMarks());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private final CustomSearchView.OnSearchListener onSearchListener = new CustomSearchView.OnSearchListener() {
        @Override
        public void onSearch(final String keyword) {
            Log.d(TAG, "startSearch keyword:" + keyword);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    WebViewActivity.openWebPageByUrl(IndexActivity.this, keyword);
                }
            });
        }
    };

    private final AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "onItemClickListener onItemClick position:" + position + ",id:" + id);
            try {
                BookMark bookMark = bookMarkListAdapter.getItem(position);
                if (bookMark != null) {
                    final String url = bookMark.getUrl();
                    Log.d(TAG, "onItemClickListener onItemClick openWebPageByUrl:" + url);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            WebViewActivity.openWebPageByUrl(IndexActivity.this, url);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private final Handler handler = new Handler(Looper.getMainLooper());

}

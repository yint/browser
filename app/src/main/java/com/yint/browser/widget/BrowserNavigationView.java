package com.yint.browser.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.yint.browser.R;

public class BrowserNavigationView extends LinearLayout {

    private final String TAG = "BrowserNavigationView";

    public BrowserNavigationView(Context context) {
        this(context, null);
    }

    public BrowserNavigationView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BrowserNavigationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.browser_navigation_layout,this);
    }
}

package com.yint.browser.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yint.browser.R;

/**
 * 自定义搜索控件
 */
public class CustomSearchView extends LinearLayout {

    private final String TAG = "CustomSearchView";

    private ImageView img_search;//左侧搜索图标
    private EditText et_search; //输入框
    private ImageView iv_clear; //右侧删除图标
    private String searchText; //搜索词
    private String hintText; //提示词
    private float editTextSize = 14;
    private boolean hideImg; //隐藏左侧搜索图标

    private OnSearchListener onSearchListener;

    public CustomSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
        initAttrs(context, attrs);
        bindView();
    }

    private void initView(Context context) {
        //加载布局文件
        View view = LayoutInflater.from(context).inflate(R.layout.view_search, null);

        //获取控件
        img_search = (ImageView) view.findViewById(R.id.img_search);
        et_search = (EditText) view.findViewById(R.id.et_search);
        iv_clear = (ImageView) view.findViewById(R.id.iv_clear);

        //文字改变监听
        et_search.addTextChangedListener(textWatcher);
        //设置事件监听
        et_search.setOnEditorActionListener(onEditorActionListener);
        //设置清空按钮的触发器
        iv_clear.setOnClickListener(onClickListener);

        //把布局添加到当前控件中
        ViewGroup.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(view, params);
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomSearchView, 0, 0);
        int n = ta.getIndexCount();
        for (int i = 0; i < n; i++) {
            int index = ta.getIndex(i);
            if (index == R.styleable.CustomSearchView_sv_textSize) {
                editTextSize = ta.getDimension(R.styleable.CustomSearchView_sv_textSize, editTextSize);
            } else if (index == R.styleable.CustomSearchView_sv_text) {
                searchText = ta.getString(R.styleable.CustomSearchView_sv_text);
            } else if (index == R.styleable.CustomSearchView_sv_hint) {
                hintText = ta.getString(R.styleable.CustomSearchView_sv_hint);
            } else if (index == R.styleable.CustomSearchView_sv_hideImg) {
                hideImg = ta.getBoolean(R.styleable.CustomSearchView_sv_hideImg, false);
            }
        }
        ta.recycle();//回收释放
    }

    private void bindView() {
        //文字大小
        Log.d(TAG, "bindView editTextSize:" + editTextSize);
        et_search.setTextSize(TypedValue.COMPLEX_UNIT_SP, editTextSize);
        //搜索框文字
        Log.d(TAG, "bindView searchText:" + searchText);
        if (searchText != null) {
            et_search.setText(searchText);
        }
        //提示文字
        Log.d(TAG, "bindView hintText:" + hintText);
        if (hintText != null) {
            et_search.setHint(hintText);
        }
        //是否隐藏搜索图标
        Log.d(TAG, "bindView hideImg:" + hideImg);
        if (hideImg) {
            img_search.setVisibility(GONE);
        }
    }

    public void setOnSearchListener(OnSearchListener onSearchListener) {
        this.onSearchListener = onSearchListener;
    }

    /**
     * 获取搜索框的文字
     */
    public String getText() {
        return et_search.getText().toString();
    }

    private void startSearch() {
        String keyword = et_search.getText().toString().trim();
        Log.d(TAG, "startSearch keyword:" + keyword);
        if (onSearchListener != null) {
            onSearchListener.onSearch(keyword);
        }
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            //删除图标
            if (s.toString().isEmpty()) {
                iv_clear.setVisibility(GONE);
            } else {
                iv_clear.setVisibility(VISIBLE);
            }
        }
    };

    private final OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v == iv_clear) {
                et_search.setText("");
            }
        }
    };

    private final TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            Log.d(TAG, "onEditorActionListener onEditorAction actionId:" + actionId);
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                startSearch();
            }
            return false;
        }
    };

    public interface OnSearchListener {
        void onSearch(String keyword);
    }

}